set -gx PATH $HOME/.fnm $PATH;
set -gx PATH $HOME/.fnm/current/bin $PATH;
set -gx FNM_MULTISHELL_PATH $HOME/.fnm/current;
set -gx FNM_DIR $HOME/.fnm/;
set -gx FNM_NODE_DIST_MIRROR https://nodejs.org/dist
set -gx FZF_DEFAULT_COMMAND 'fd --type f'
