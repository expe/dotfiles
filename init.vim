" Specify a directory for plugins (for Neovim: ~/.local/share/nvim/plugged)
call plug#begin('~/.local/share/nvim/plugged')

" Plug 'floobits/floobits-neovim'
Plug 'scrooloose/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" Plug 'itchyny/lightline.vim'
Plug 'Yggdroot/indentLine'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'w0rp/ale'
" Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }

"CoC
Plug 'neoclide/coc.nvim', {'tag': '*', 'do': { -> coc#util#install()}}
" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> for trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> for confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[c` and `]c` for navigate diagnostics
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K for show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if &filetype == 'vim'
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)

" Remap for format selected region
vmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)

" Fix autofix problem of current line
nmap <leader>f  <Plug>(coc-fix-current)

" Better display for messages
set cmdheight=2
set updatetime=300

" don't give |ins-completion-menu| messages.
set shortmess+=c

" always show signcolumns
set signcolumn=yes

set modelines=0
set nomodeline

Plug 'majutsushi/tagbar'
" Plug 'ludovicchabant/vim-gutentags'
" Plug 'jsfaint/gen_tags.vim'
Plug 'tpope/vim-commentary'
"Plug 'godlygeek/tabular'
Plug 'junegunn/vim-easy-align'
Plug 'mg979/vim-visual-multi'
" Plug 'terryma/vim-multiple-cursors'
" Plug 'vim-scripts/ReplaceWithRegister'

" Themes
Plug 'joshdick/onedark.vim'
Plug 'chriskempson/base16-vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'rakr/vim-one'
Plug 'gruvbox-community/gruvbox'
Plug 'aonemd/kuroi.vim'
Plug 'arcticicestudio/nord-vim'
Plug 'rakr/vim-one'
Plug 'jacoborus/tender'
Plug 'romainl/Apprentice'
" Plug 'tpope/vim-sensible'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/gv.vim'
Plug 'airblade/vim-gitgutter'
Plug 'mxw/vim-jsx'
Plug 'tpope/vim-surround'
let g:surround_indent = 0

Plug 'tpope/vim-repeat'
Plug 'mattn/emmet-vim'
Plug 'tpope/vim-sleuth'
Plug 'elzr/vim-json'
Plug 'tpope/vim-jdaddy'

" Plug 'jordwalke/vim-reasonml'
Plug 'jparise/vim-graphql'
" Plug 'freebroccolo/ocaml-language-server'
Plug 'reasonml-editor/vim-reason-plus'
Plug 'elmcast/elm-vim'
let g:elm_format_autosave = 1

Plug 'pangloss/vim-javascript'
" Plug 'ryanoasis/vim-devicons'
" Plug '~/.opam/4.07.1/share/merlin/vim'
call plug#end()

" set completeopt-=preview
set noswapfile     " Don't make backups.
set nowritebackup " Even if you did make a backup, don't keep it around.
set nobackup
set noshowmode " Hide the mode text (e.g. -- INSERT --)
set nojoinspaces
set nostartofline
" Theme
set background=dark
set termguicolors
syntax enable
" don't syntax-highlight long lines
set synmaxcol=200
let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1

colorscheme onedark
let g:nord_italic = 1
let g:airline_theme = 'onedark'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#show_buffers = 1

" let g:lightline = {
"       \ 'colorscheme': 'nord',
"       \ 'active': {
"       \   'left': [ [ 'mode', 'paste' ],
"       \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
"       \ },
"       \ 'component_function': {
"       \   'gitbranch': 'fugitive#head'
"       \ },
"       \ }
" set showtabline=2
" set laststatus=2

" lint
let g:ale_sign_column_always = 1
let g:ale_maximum_file_size = 100000
let g:ale_lint_delay = 1000

filetype plugin on

set clipboard=unnamedplus
set virtualedit=block


" Indent
set expandtab shiftwidth=2 softtabstop=-1
" set listchars=tab:▏\ ,eol:\ ,extends:,precedes:,space:\ ,trail:⋅
set list
" set tabstop=2
" set softtabstop=2
" set shiftwidth=2
" set expandtab


set nonumber
set noswapfile
set mouse=a " don't copy UI characters
set lazyredraw
set inccommand=nosplit
filetype plugin indent on
" set foldmethod=indent

" fix shell
if &shell =~# 'fish$'
    set shell=sh
endif

" Neovim settings
set autoread
set encoding=utf8


" let g:indentLine_enabled = 1
" let g:indentLine_char = '▏'
" let g:indentLine_color_gui = '#3A3A3A'
let g:vim_json_syntax_conceal = 0 
nnoremap <silent> <Leader>j :%!python -m json.tool<CR>

" SPLITS
set noequalalways
set splitright
set splitbelow

set scrolloff=5
" set number
set relativenumber number
set showcmd
set cursorline
set hidden
" set list
" set listchars=eol:!,tab:>=,trail:.
" set list listchars=tab:>-,trail:.,extends:>,precedes:<,space:.
" set listchars=tab:\|\ ,
let g:gitgutter_realtime = 1

" Editing
set wrap "dont wrap lines
set linebreak "wrap lines at convenient points


set formatoptions+=rno1l
" don't syntax-highlight long lines
set synmaxcol=200

" Searching
set ignorecase smartcase
nnoremap <silent> <Leader>n :noh<CR>

" Moving lines
nnoremap <silent> <C-k> :move-2<cr>
nnoremap <silent> <C-j> :move+<cr>
nnoremap <silent> <C-h> <<
nnoremap <silent> <C-l> >>

"make wrapped lines more intuitive
noremap <silent> k gk
noremap <silent> j gj
noremap <silent> 0 g0
noremap <silent> $ g$

" FZF
map <C-p> :Files<cr>
nmap <C-p> :Files<cr>
nnoremap <silent> <Leader>rg :Rg <C-R><C-W><CR>
nnoremap <silent> <Leader>RG :Rg <C-R><C-A><CR>
nnoremap <silent> <Leader>` :Marks<CR>
nnoremap <silent> <Leader>b :Buffers<CR>


nmap <F1> <nop>
imap <F1> <nop>

command! -bang -nargs=* Rg
  \ call fzf#vim#grep(
  \   'rg -i --column --line-number --no-heading --color=always '.shellescape(<q-args>), 1,
  \   <bang>0 ? fzf#vim#with_preview('up:60%')
  \           : fzf#vim#with_preview('right:50%:hidden', '?'),
  \   <bang>0)


" <F10> | NERD Tree
nnoremap <F10> :NERDTreeToggle<cr>

" Buffer management
" `gb` switches to next buffer, like `gt` does with tabs
nnoremap gb :bn<CR>
" `gB` switches to previous buffer, like `gT` does with tabs
nnoremap gB :bp<CR>
nnoremap <Leader>q :bd<CR>

" Tags
" set tags=./tags;/
"set tags+=tags;$HOME
" let g:gutentags_file_list_command = 'rg --files'
" nmap <F8> :TagbarToggle<CR>
"
" Easy align
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)" Specify a directory for plugins (for Neovim: ~/.local/share/nvim/plugged)

" set rtp^="/home/exp/.opam/4.07.1/share/ocp-indent/vim"

